<?php

namespace Dreamcat\Components\QuickMock\Resource;

/**
 * -
 * @author vijay
 */
class Helper
{
    public static function compareObj($obj1, $obj2)
    {
        if (function_exists("spl_object_id")) {
            return spl_object_id($obj1) == spl_object_id($obj2);
        } else {
            return $obj1 === $obj2 && spl_object_hash($obj1) == spl_object_hash($obj2);
        }
    }
}

# end of file
