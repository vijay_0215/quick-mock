<?php

namespace Dreamcat\Components\QuickMock\Resource;

use Exception;
use ReflectionMethod;

/**
 * -
 * @author vijay
 */
class ProxyInvokeParams extends Exception
{
    public $obj;
    /** @var ReflectionMethod  */
    public $method;
    public $args;

    public function __construct($object, ReflectionMethod $method, $args)
    {
        parent::__construct();
        $this->obj = $object;
        $this->method = $method;
        $this->args = $args;
    }
}

# end of file
