<?php

namespace Dreamcat\Components\QuickMock\Resource;

use Dreamcat\Components\QuickMock\Proxy\ProxyInterface;
use ReflectionMethod;

/**
 * -
 * @author vijay
 */
class DemoProxy implements ProxyInterface
{
    /**
     * @inheritDoc
     * @throws ProxyInvokeParams
     */
    public function invoke($object, ReflectionMethod $method, array $args)
    {
        throw new ProxyInvokeParams($object, $method, $args);
    }
}

# end of file
