<?php /** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpDocSignatureInspection */
/** @noinspection PhpLanguageLevelInspection */

/** @noinspection Annotator */

namespace Dreamcat\Components\QuickMock\Resource;

interface DemoInterface
{
    /**
     * -
     * @return mixed
     */
    public function testFunction();

    /**
     * -
     * @param DemoInterface $demo
     * @param array $arg
     * @param string $def
     * @return mixed
     */
    public function testWithParm(DemoInterface $demo, array $arg, string $def = "123"): string;

    /**
     * -
     * @param callable $fn
     * @param int $int
     * @param string $s
     * @param array $adef
     * @param float $float
     * @param string ...$arg
     * @return mixed
     */
    public function testOtherParam(
        callable $fn,
        int $int,
        $s = "1234",
        $adef = [],
        float &$float = 3,
        string &...$arg
    ): ?DemoInterface;
}

# end of file
