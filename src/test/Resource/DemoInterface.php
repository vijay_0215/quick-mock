<?php /** @noinspection PhpUndefinedClassInspection */

namespace Dreamcat\Components\QuickMock\Resource;

if (version_compare(phpversion(), "7.0.0") >= 0) {
    # 7.0之后的版本
    include_once __DIR__ . "/DemoInterfaceFor7.php";
} else {
    interface DemoInterface
    {
        /**
         * -
         * @return mixed
         */
        public function testFunction();

        /**
         * -
         * @param DemoInterface $demo
         * @param $arg
         * @param string $def
         * @return mixed
         */
        public function testWithParm(DemoInterface $demo, $arg, $def = "123");

        /**
         * -
         * @param callable $fn
         * @param $int
         * @param string $s
         * @param array $adef
         * @param int $float
         * @param mixed ...$arg
         * @return mixed
         */
        public function testOtherParam(callable $fn, $int, $s = "1234", $adef = [], &$float = 3, &...$arg);
    }
}

# end of file
