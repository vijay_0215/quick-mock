<?php

namespace Dreamcat\Components\QuickMock;

use Dreamcat\Components\QuickMock\Proxy\ProxyInterface;

/**
 * mock工厂
 * @author vijay
 */
interface MockFactory
{
    /**
     * 创建mock
     * @param string $interfaceName 接口名
     * @param ProxyInterface $proxyClass 代理类对象
     * @return mixed 实现了$interfaceName接口的实例
     */
    public function createMock($interfaceName, ProxyInterface $proxyClass);
}

# end of file
