<?php

namespace Dreamcat\Components\QuickMock\Error;

use BadFunctionCallException;

/**
 * mock工厂异常
 * @author vijay
 */
class MockFactoryException extends BadFunctionCallException
{
    /** @var int 接口不存在 */
    const INTERFACE_NOT_EXISTS = 1;
    /** @var int 静态方法不支持 */
    const STATIC_METHOD_NOT_SUPPORT = 2;
    /** @var int 生成的代码未成功 */
    const EVAL_FAILED = 3;
}

# end of file
