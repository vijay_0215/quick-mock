<?php

namespace Dreamcat\Components\QuickMock\Proxy;

use ReflectionMethod;

/**
 * 代理类接口
 * @author vijay
 */
interface ProxyInterface
{
    /**
     * 调用方法
     * @param mixed $object 被调用的对象
     * @param ReflectionMethod $method 被调用方法的反射
     * @param array $args 参数列表
     * @return mixed
     */
    public function invoke($object, ReflectionMethod $method, array $args);
}

# end of file
