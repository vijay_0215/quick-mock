# 项目简介

为一个接口快速实现一个 mock 类，用来解决代理模式的问题

## 安装

```
composer require "dreamcat/quick-mock"
```

## 使用说明
### 简单示例

## 改造说明
本项目目前是兼容 7.4 和 5.6 的版本，用例需要在两个版本下测试

各版本需要使用 PHPUnit 版本，需要自行在 composer.json 中更换

 PHP 版本 | 建议的 PHPUnit 版本
 --- | ---
 5.6 | 5
 7.4 | 9

## todo
